const Controller = {}
const db = require('./../models/index')
//const Queue = db.sequelize.models.Queue
const Template = db.sequelize.models.Template

Controller.findAllTemplate = function(req, res, next) {
    return Template.findAll({
        raw: true
    })
    .then(function(result){
        res.result = result
        return next()
    })
    .catch(function(err){
        res.error = err
        return next()
    })
}

Controller.findOneTemplate = function(req, res, next){
    return Template.findOne({
        where: {templateName: req.body.templateName},
        raw:true
    })
    .then(function(result){
        res.template = result
        return next()
    })
    .catch(function(err){
        res.error = err
        return next()
    })
}

Controller.createTemplate = function (req, res, next) {
    return Template.create({
        templateName: req.body.templateName, 
        templateId: req.body.templateId, 
        recordStatus: true
    })
    .then(function(result){
        res.create = result
        return next()
    })
    .catch(function(err){
        res.error = err
        return next()
    })
}

module.exports = Controller