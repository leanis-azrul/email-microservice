'use strict';
module.exports = (sequelize, DataTypes) => {
  const Queue = sequelize.define('Queue', {
    emailAddress: DataTypes.STRING,
    type: DataTypes.INTEGER,
    reqBody: DataTypes.JSON,
    sendStatus: DataTypes.BOOLEAN
  }, {});
  Queue.associate = function(models) {
    // associations can be defined here
  };
  return Queue;
};