'use strict';
module.exports = (sequelize, DataTypes) => {
  const Template = sequelize.define('Template', {
    templateName: DataTypes.STRING,
    templateId: DataTypes.STRING,
    recordStatus: DataTypes.BOOLEAN
  }, {});
  Template.associate = function(models) {
    // associations can be defined here
  };
  return Template;
};