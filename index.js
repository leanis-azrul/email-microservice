// Initialize dependencies
const express = require('express')
const dotenv = require('dotenv')
const Sequelize = require('sequelize')

// Load environmental variables
dotenv.config({ path: '.env' })
const port = process.env.EXPRESS_PORT
var route = require('./routes/route.js')
const env = process.env.NODE_ENV || 'development'
const config = require('./config/config.json')[env]

// Initialize app
const app = express()

//Initialize Database
let sequelize = new Sequelize(config.database, config.username, config.password, config)

//Testing DB Connection
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection to ' + config.database + ' has been established successfully.')
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err)
  });


// JSON parser
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Define root route
app.get('/', function(req, res) {
  res.status(200).send('LeanPay Email Microservice')
});

// Define routing files
app.use('/api/v1', route)

app.use(function(req, res, next) {
  const error = new Error('Invalid Request')
  error.status = 404;
  next(error);
});

app.use(function(error, req, res, next) {
  if (error instanceof SyntaxError) {
      return res.status(400).json({
          status: 'failed',
          status_code: 400,
          status_description: 'Invalid JSON'
      });
  }

  res.status(error.status | 500).json({
      status: 'failed',
      status_code: error.status,
      status_description: error.message
  });
});

//Start Server
app.listen(port, () => { 
    console.log(`Listening on port ${port}!`) 
})