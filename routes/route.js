// Initialize dependencies
const express = require('express')
const sgMail = require('@sendgrid/mail')
const fs = require('fs')
const { check, validationResult } = require('express-validator')

// Load environmental variables
const router = express.Router()
const AuthMiddleware = require('./../middlewares/auth')
const Controller = require('./../controllers/controller')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const path = "./../generatedPdf/"

// Route
router.post('/sendPlainMail', AuthMiddleware.verifyClient, [
    check('to').isEmail(),
],(req, res) => 
{
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        res.status(422).json({
            status: 'failed',
            status_code: 422,
            error: process.env.ERR_REQUIREMENT
        })
    }

    const msg = {
        to: req.body.to,
        from: process.env.FROM_VALUE,
        subject: req.body.subject,
        text: req.body.text
        //html: '<strong>and easy to do anywhere, even with Node.js</strong>',
      };
      sgMail
        .send(msg)
        .then(() => {
            return res.status(201).json({
                status: 'successful',
                status_code: 201,
                data: 'Email successfully sent'
            })
        })
        .catch((err) => {
            return res.status(422).json({
                status: 'failed',
                status_code: 422,
                error: err
            })
        })
})

router.post('/sendHTMLMail', AuthMiddleware.verifyClient, [
    check('to').isEmail(),
],(req, res) => 
{
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        res.status(422).json({
            status: 'failed',
            status_code: 422,
            error: process.env.ERR_REQUIREMENT
        })
    }

    const msg = {
        to: req.body.to,
        from: process.env.FROM_VALUE,
        subject: req.body.subject,
        html: req.body.html,
      };
      sgMail
        .send(msg)
        .then(() => {
            return res.status(201).json({
                status: 'successful',
                status_code: 201,
                data: 'Email successfully sent'
            })
        })
        .catch((err) => {
            return res.status(422).json({
                status: 'failed',
                status_code: 422,
                error: err
            })
        })
})

router.post('/sendAttachmentPlainMail', AuthMiddleware.verifyClient, [
    check('to').isEmail()
], (req,res) => {

    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        res.status(422).json({
            status: 'failed',
            status_code: 422,
            error: process.env.ERR_REQUIREMENT
        })
    }
    
    const filename = req.body.pdfReference
    const pathToAttachment = path + filename + '.pdf'
    const attachment = fs.readFileSync(pathToAttachment).toString("base64")

    const msg = {
        to: req.body.to,
        from: process.env.FROM_VALUE,
        subject: req.body.subject,
        text: req.body.text,
        attachments: [
            {
              content: attachment,
              filename: filename + ".pdf",
              type: "application/pdf",
              disposition: "attachment"
            }
          ]
      };
      sgMail
        .send(msg)
        .then(() => {
            return res.status(201).json({
                status: 'successful',
                status_code: 201,
                data: 'Email with attachment successfully sent'
            })
        })
        .catch((err) => {
            return res.status(422).json({
                status: 'failed',
                status_code: 422,
                error: err
            })
        })
})

router.post('/sendAttachmentHtmlMail', AuthMiddleware.verifyClient, [
    check('to').isEmail()
], (req,res) => {

    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        res.status(422).json({
            status: 'failed',
            status_code: 422,
            error: process.env.ERR_REQUIREMENT
        })
    }
    
    const filename = req.body.pdfReference
    const pathToAttachment = path + filename + '.pdf'
    const attachment = fs.readFileSync(pathToAttachment).toString("base64")

    const msg = {
        to: req.body.to,
        from: process.env.FROM_VALUE,
        subject: req.body.subject,
        html: req.body.html,
        attachments: [
            {
              content: attachment,
              filename: filename + ".pdf",
              type: "application/pdf",
              disposition: "attachment"
            }
          ]
      };
      sgMail
        .send(msg)
        .then(() => {
            return res.status(201).json({
                status: 'successful',
                status_code: 201,
                data: 'Email with attachment successfully sent'
            })
        })
        .catch((err) => {
            return res.status(422).json({
                status: 'failed',
                status_code: 422,
                error: err
            })
        })
})

router.post('/sendTemplateMail', AuthMiddleware.verifyClient, Controller.findOneTemplate, [
    check('to').isEmail()
], (req,res) => {

    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        res.status(422).json({
            status: 'failed',
            status_code: 422,
            error: process.env.ERR_REQUIREMENT
        })
    }

    if (res.template){
        const msg = {
                to: req.body.to,
                from: process.env.FROM_VALUE,
                templateId: res.template.templateId,
                dynamic_template_data: req.body.data
            }
        
        sgMail.send(msg)
        .then(() => {
            return res.status(201).json({
                status: 'successful',
                status_code: 201,
                data: 'Email with template successfully sent'
            })
        }).catch((err) => {
            return res.status(422).json({
                status: 'failed',
                status_code: 422,
                error: err
            })
        })
    }
    else
    {
        return res.status(201).json({
            status: 'successful',
            status_code: 201,
            data: "No Template Name Found"
        })
    }
})

router.post('/sendAttachmentTemplateMail', AuthMiddleware.verifyClient, Controller.findOneTemplate, [
    check('to').isEmail()
], (req,res) => {

    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        res.status(422).json({
            status: 'failed',
            status_code: 422,
            error: process.env.ERR_REQUIREMENT
        })
    }

    const filename = req.body.pdfReference
    const pathToAttachment = path + filename + '.pdf'
    const attachment = fs.readFileSync(pathToAttachment).toString("base64")

    if (res.template){
        const msg = {
                to: req.body.to,
                from: process.env.FROM_VALUE,
                templateId: res.template.templateId,
                dynamic_template_data: req.body.data,
                attachments: [
                    {
                      content: attachment,
                      filename: filename + ".pdf",
                      type: "application/pdf",
                      disposition: "attachment"
                    }
                  ]
            }
        
        sgMail.send(msg)
        .then(() => {
            return res.status(201).json({
                status: 'successful',
                status_code: 201,
                data: 'Email with template successfully sent'
            })
        }).catch((err) => {
            return res.status(422).json({
                status: 'failed',
                status_code: 422,
                error: err
            })
        })
    }
    else
    {
        return res.status(201).json({
            status: 'successful',
            status_code: 201,
            data: "No Template Name Found"
        })
    }
})

router.get('/getTemplateList', AuthMiddleware.verifyClient, Controller.findAllTemplate, (req, res) => {
    if(res.error)
    {
        return res.status(422).json({
            status: 'failed',
            status_code: 422,
            error: res.error
        })
    } 
    else {
        return res.status(201).json({
            status: 'successful',
            status_code: 201,
            data: res.result
        })
    }
})

router.post('/addNewTemplate', AuthMiddleware.verifyClient, Controller.createTemplate, (req, res) => {
    if(res.error)
    {
        return res.status(422).json({
            status: 'failed',
            status_code: 422,
            error: res.error
        })
    } 
    else {
        return res.status(201).json({
            status: 'successful',
            status_code: 201,
            data: res.create
        })
    }
})

module.exports = router;